import React from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

const CommentForm = props => {
    return (
        <Form horizontal onSubmit={props.submitFormHandler}>
            <FormGroup controlId="productTitle">
                <Col componentClass={ControlLabel} sm={2}>
                    Title
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        placeholder="Author"
                        name="author"
                        value={props.author}
                        onChange={props.changeHandler}
                    />
                </Col>
            </FormGroup>

            <FormGroup controlId="productDescription">
                <Col componentClass={ControlLabel} sm={2}>
                    Description
                </Col>
                <Col sm={10}>
                    <FormControl
                        componentClass="textarea" required
                        placeholder="Enter comment"
                        name="comment"
                        value={props.comment}
                        onChange={props.changeHandler}
                    />
                </Col>
            </FormGroup>

            <FormGroup>
                <Col smOffset={2} sm={10}>
                    <Button bsStyle="primary" type="submit">Save</Button>
                </Col>
            </FormGroup>
        </Form>
    );
};

export default CommentForm;