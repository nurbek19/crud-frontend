import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import NewPost from "./containers/NewPost/NewPost";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import News from "./containers/News/News";
import SinglePost from "./containers/SinglePost/SinglePost";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header><Toolbar/></header>
                <main className="container">
                    <Switch>
                        <Route path="/" exact component={News} />
                        <Route path="/news/new" exact component={NewPost}/>
                        <Route path="/news/:id" exact component={SinglePost}/>
                    </Switch>
                </main>
            </Fragment>
        );
    }
}

export default App;
