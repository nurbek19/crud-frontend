import axios from '../axios-api';

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_ERROR = 'FETCH_NEWS_ERROR';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';
export const FETCH_SINGLE_NEWS_SUCCESS = 'FETCH_SINGLE_NEWS_SUCCESS';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';


export const fetchNewsRequest = () => {
  return {type: FETCH_NEWS_REQUEST};
};

export const fetchNewsSuccess = news => {
    return {type: FETCH_NEWS_SUCCESS, news}
};

export const fetchNewsError = () => {
  return {type: FETCH_NEWS_ERROR};
};

export const fetchNews = () => {
    return dispatch => {
        dispatch(fetchNewsRequest());
        return axios.get('/news').then(response => {
            dispatch(fetchNewsSuccess(response.data));
        }, error => {
            dispatch(fetchNewsError());
        })
    }
};

export const createNewsSuccess = () => {
    return {type: CREATE_NEWS_SUCCESS}
};

export const createNews = newsData => {
    return dispatch => {
        return axios.post('/news', newsData).then(() => {
            dispatch(createNewsSuccess());
        })
    }
};

export const deleteNewsSuccess = () => {
  return {type: DELETE_NEWS_SUCCESS}
};

export const deleteNews = id => {
  return dispatch => {
      return axios.delete(`/news/${id}`).then(() => {
          dispatch(deleteNewsSuccess());
      })
  }
};

export const fetchSingleNewsSuccess = singleNews => {
    return {type: FETCH_SINGLE_NEWS_SUCCESS, singleNews}
};

export const fetchSingleNews = id => {
    return dispatch => {
        return axios.get(`/news/${id}`).then(response => {
            dispatch(fetchSingleNewsSuccess(response.data));
        });
    }
};

export const fetchCommentsSuccess = comments => {
    return {type: FETCH_COMMENTS_SUCCESS, comments}
};

export const fetchComments = id => {
    return dispatch => {
        axios.get(`/comments?news_id=${id}`).then(response => {
            dispatch(fetchCommentsSuccess(response.data));
        })
    }
};

export const createCommentSuccess = () => {
  return {type: CREATE_COMMENT_SUCCESS};
};

export const createComment = commentData => {
    return dispatch => {
        return axios.post('/comments', commentData).then(() => {
            dispatch(createCommentSuccess());
        })
    }
};

export const deleteCommentSuccess = () => {
  return {type: DELETE_COMMENT_SUCCESS};
};

export const deleteComment = id => {
  return dispatch => {
      return axios.delete(`/comments/${id}`).then(() => {
          dispatch(deleteCommentSuccess())
      })
  }
};

