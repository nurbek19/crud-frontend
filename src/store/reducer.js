import {
    FETCH_COMMENTS_SUCCESS,
    FETCH_NEWS_ERROR,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS,
    FETCH_SINGLE_NEWS_SUCCESS
} from "./actions";

const initialState = {
    news: [],
    singleNews: null,
    comments: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return {...state, loading: true};
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.news, loading:false};
        case FETCH_NEWS_ERROR:
            return {...state, loading: false};
        case FETCH_SINGLE_NEWS_SUCCESS:
            return {...state, singleNews: action.singleNews};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};
        default:
            return state;
    }
};

export default reducer;