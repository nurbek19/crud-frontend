import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Grid, Image, Panel, Row} from "react-bootstrap";
import {createComment, deleteComment, fetchComments, fetchSingleNews} from "../../store/actions";
import CommentForm from "../../components/CommentForm/CommentForm";


class SinglePost extends Component {
    state = {
        author: '',
        comment: ''
    };

    componentDidMount() {
        this.props.onFetchSingleNews(this.props.match.params.id).then(() => {
            this.props.onFetchComments(this.props.match.params.id)
        });

    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const comment = this.state;
        comment.news_id = this.props.match.params.id;

        this.props.onCommentCreated(comment).then(() => {
            this.props.onFetchComments(this.props.match.params.id);
        });
    };

    deleteComment = id => {
      this.props.onDeletedComment(id).then(() => {
          this.props.onFetchComments(this.props.match.params.id);
      });
    };

    render() {
        const singleNews = this.props.singleNews;

        if(singleNews !== null) {
            return(
                <Grid>
                    <Row className="show-grid">
                        <h2>{singleNews.title}</h2>
                        <span style={{display: 'block', margin: '10px 0'}}>{singleNews.publication_date}</span>

                        {singleNews.image &&
                        <Image src={'http://localhost:8000/uploads/' + singleNews.image} rounded />
                        }

                        <p style={{margin: '10px 0 30px'}}>{singleNews.description}</p>
                    </Row>

                    <Row className="show-grid">
                        {this.props.comments.map(comment => (
                            <Panel key={comment.id}>
                                <Panel.Body>
                                    <h4>{comment.author} wrote:</h4>
                                    <p>{comment.comment}</p>
                                    <Button bsStyle="danger" className="pull-right" onClick={() => this.deleteComment(comment.id)}>Delete</Button>
                                </Panel.Body>
                            </Panel>
                        ))}
                    </Row>

                    <Row className="show-grid">
                        <CommentForm
                            author={this.state.author}
                            comment={this.state.comment}
                            changeHandler={this.inputChangeHandler}
                            submitFormHandler={this.submitFormHandler}
                        />
                    </Row>
                </Grid>
            )
        } else {
            return <p style={{fontSize: '100px', textAlign: 'center'}}>Loading...</p>
        }

    }
}


const mapStateToProps = state => {
    return {
        singleNews: state.singleNews,
        comments: state.comments
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchSingleNews: id => dispatch(fetchSingleNews(id)),
        onFetchComments: id => dispatch(fetchComments(id)),
        onCommentCreated: commentData => dispatch(createComment(commentData)),
        onDeletedComment: id => dispatch(deleteComment(id))
    }
};

export default connect (mapStateToProps, mapDispatchToProps)(SinglePost);