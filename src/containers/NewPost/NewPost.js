import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import PostForm from "../../components/PostForm/PostForm";
import {createNews} from "../../store/actions";

class NewPost extends Component {

    createNews = newsData => {
        this.props.onNewsCreated(newsData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return(
            <Fragment>
                <PageHeader>New  post</PageHeader>
                <PostForm onSubmit={this.createNews} />
            </Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onNewsCreated: newsData => dispatch(createNews(newsData))
    }
};

export default connect(null, mapDispatchToProps)(NewPost);