import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Grid, Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import {deleteNews, fetchNews} from "../../store/actions";

class Products extends Component {
    componentDidMount() {
        this.props.onFetchNews();
    }

    deleteNews = id => {
        this.props.onDeleteNews(id).then(() => {
            this.props.onFetchNews();
        });
    };

    render() {
        let news = (this.props.news.map(news => (
            <Panel key={news.id}>
                <Panel.Body>
                    <Col xs={12} md={3}>
                        {news.image &&
                        <Image src={'http://localhost:8000/uploads/' + news.image} thumbnail/>
                        }
                    </Col>
                    <Col xs={12} md={9}>
                        <h4>{news.title}</h4>
                        <p>{news.publication_date}</p>
                        <Link to={"/news/" + news.id}>Read full post</Link>
                        <Button bsStyle="danger" className="pull-right"
                                onClick={() => this.deleteNews(news.id)}>Delete</Button>
                    </Col>
                </Panel.Body>
            </Panel>
        )));

        if (this.props.loading) {
            news = <p style={{fontSize: '100px', textAlign: 'center'}}>Loading...</p>
        }

        return (
            <Grid>
                <h2>News
                    <Link to="/news/new">
                        <Button bsStyle="primary" className="pull-right">Add product</Button>
                    </Link>
                </h2>

                {news}
            </Grid>
        )
    }
}

const mapStateToProps = state => {
    return {
        news: state.news,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchNews: () => dispatch(fetchNews()),
        onDeleteNews: id => dispatch(deleteNews(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);